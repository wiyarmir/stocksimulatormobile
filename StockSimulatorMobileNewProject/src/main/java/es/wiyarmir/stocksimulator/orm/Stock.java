package es.wiyarmir.stocksimulator.orm;

import android.util.JsonReader;

import java.io.IOException;
import java.io.Serializable;


public class Stock implements Serializable {
    private String ticker;
    private String companyName;
    private int quantity;
    private double currentStockPrice;
    private double transactionStockPrice;
    private double currentAskPrice;
    private double openingStockPrice;

    public Stock(String ticker, String companyName, double currentStockPrice, int quantity) {
        this.ticker = ticker;
        this.companyName = companyName;
        this.currentStockPrice = currentStockPrice;
        this.quantity = quantity;
    }

    public Stock() {
        this("", "", 0, 0);
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getCurrentStockPrice() {
        return currentStockPrice;
    }

    public void setCurrentStockPrice(double currentStockPrice) {
        this.currentStockPrice = currentStockPrice;
    }

    public double getTransactionStockPrice() {
        return transactionStockPrice;
    }

    public void setTransactionStockPrice(double transactionStockPrice) {
        this.transactionStockPrice = transactionStockPrice;
    }

    public double getCurrentAskPrice() {
        return currentAskPrice;
    }

    public void setCurrentAskPrice(double currentAskPrice) {
        this.currentAskPrice = currentAskPrice;
    }


    public double getOpeningStockPrice() {
        return openingStockPrice;
    }

    public void setOpeningStockPrice(double openingStockPrice) {
        this.openingStockPrice = openingStockPrice;
    }

    public static Stock readStock(JsonReader reader) throws IOException {
        Stock c = new Stock();
        reader.beginObject();
        while (reader.hasNext()) {

            String name = reader.nextName();
            if (name.equals("ticker")) {
                c.setTicker(reader.nextString());
            } else if (name.equals("companyName")) {
                c.setCompanyName(reader.nextString());
            } else if (name.equals("quantity")) {
                c.setQuantity((int) reader.nextDouble());
            } else if (name.equals("currentBidPrice")) {
                c.setCurrentStockPrice(reader.nextDouble());
            } else if (name.equals("currentAskPrice")) {
                c.setCurrentAskPrice(reader.nextDouble());
            } else if (name.equals("openingStockPrice")) {
                c.setOpeningStockPrice(reader.nextDouble());
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return c;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = ticker != null ? ticker.hashCode() : 0;
        result = 31 * result + (companyName != null ? companyName.hashCode() : 0);
        result = 31 * result + quantity;
        temp = Double.doubleToLongBits(currentStockPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(transactionStockPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(currentAskPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(openingStockPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return getTicker();
    }
}
