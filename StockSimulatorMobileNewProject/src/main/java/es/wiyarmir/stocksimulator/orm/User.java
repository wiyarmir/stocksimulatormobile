package es.wiyarmir.stocksimulator.orm;

import android.util.JsonReader;

import java.io.IOException;

public class User {
    String username;
    double score;
    double accountBalance;
    double penalties;
    double totalValue;
    int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public double getPenalties() {
        return penalties;
    }

    public void setPenalties(double penalties) {
        this.penalties = penalties;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public static User readUser(JsonReader reader) throws IOException {
        User c = new User();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("username")) {
                c.setUsername(reader.nextString());
            } else if (name.equals("userId")) {
                c.setUserId(reader.nextInt());
            } else if (name.equals("score")) {
                c.setScore(reader.nextDouble());
            } else if (name.equals("accountBalance")) {
                c.setAccountBalance(reader.nextDouble());
            } else if (name.equals("penalties")) {
                c.setPenalties(reader.nextDouble());
            } else if (name.equals("totalValue")) {
                c.setTotalValue(reader.nextDouble());
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return c;
    }

    @Override
    public String toString() {
        return getUsername();
    }
}
