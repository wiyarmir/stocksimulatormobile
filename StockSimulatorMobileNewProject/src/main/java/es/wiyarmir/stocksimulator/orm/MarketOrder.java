package es.wiyarmir.stocksimulator.orm;


public class MarketOrder {
    private String ticker;
    private int quantity;
    private TransactionType transactionType;

    public MarketOrder(String ticker, int quantity, TransactionType transactionType) {
        setTicker(ticker);
        setQuantity(quantity);
        setTransactionType(transactionType);
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getTicker() {
        return ticker;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }
}
