package es.wiyarmir.stocksimulator.orm;

import android.util.JsonReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Portfolio {
    private List<Stock> stocksList = new ArrayList<Stock>();
    private double accountBalance = 0, accountPenalty = 0;
    private User user = new User();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Stock> getStocksList() {
        return stocksList;
    }

    public void setStocksList(List<Stock> stocksList) {
        this.stocksList = stocksList;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public double getAccountPenalty() {
        return accountPenalty;
    }

    public void setAccountPenalty(double accountPenalty) {
        this.accountPenalty = accountPenalty;
    }

    public static Portfolio readPortfolio(JsonReader reader) throws IOException {
        Portfolio p = new Portfolio();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                //FIXME return to righ tvalue when services change
                //p.getUser().setUserId(reader.nextInt());
                reader.skipValue();
            } else if (name.equals("username")) {
                p.getUser().setUsername(reader.nextString());
            } else if (name.equals("accountBalance")) {
                p.setAccountBalance(reader.nextDouble());
            } else if (name.equals("penalty")) {
                p.setAccountPenalty(reader.nextDouble());
            } else if (name.equals("stocks")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    p.getStocksList().add(Stock.readStock(reader));
                }
                reader.endArray();
            } else {
                reader.skipValue();
            }
        }

        return p;
    }
}

