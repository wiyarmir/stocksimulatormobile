package es.wiyarmir.stocksimulator.orm;

import android.util.JsonReader;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;


public class Transaction implements Serializable{
    String ticker;
    String companyName;
    int quantity;
    double transactionStockPrice;
    OrderType orderType;
    TransactionType transactionType;
    Date ExecutedAt;
    int orderId;
    private int userId;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTransactionStockPrice() {
        return transactionStockPrice;
    }

    public void setTransactionStockPrice(double transactionStockPrice) {
        this.transactionStockPrice = transactionStockPrice;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getExecutedAt() {
        return ExecutedAt;
    }

    public void setExecutedAt(Date executedAt) {
        ExecutedAt = executedAt;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public static Transaction readTransaction(JsonReader reader) throws IOException {
        Transaction ret = new Transaction();

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("orderType")) {
                ret.setOrderType(reader.nextString().toUpperCase().equals("MARKET") ? OrderType.MARKET : OrderType.LIMIT);
            } else if (name.equals("ticker")) {
                ret.setTicker(reader.nextString());
            } else if (name.equals("companyName")) {
                ret.setCompanyName(reader.nextString());
            } else if (name.equals("quantity")) {
                ret.setQuantity(reader.nextInt());
            } else if (name.equals("transactionStockPrice")) {
                ret.setTransactionStockPrice(reader.nextDouble());
            } else if (name.equals("transactionType")) {
                ret.setTransactionType(reader.nextString().toUpperCase().equals("BUY") ? TransactionType.BUY : TransactionType.SELL);
            } else if (name.equals("orderId")) {
                ret.setOrderId(reader.nextInt());
            } else if (name.equals("userId")) {
                ret.setUserId(reader.nextInt());
            } else if (name.equals("executedAt")) {
                ret.setExecutedAt(new Date(reader.nextLong()));
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return ret;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return getTicker();
    }
}
