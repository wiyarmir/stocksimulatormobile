package es.wiyarmir.stocksimulator.orm;


public enum OrderType {
    LIMIT("Limit"),
    MARKET("Market");

    private String fieldDescription;

    OrderType(String text) {
        fieldDescription = text;
    }

    public String getFieldDescription() {
        return fieldDescription;
    }
}

