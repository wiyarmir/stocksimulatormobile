package es.wiyarmir.stocksimulator.orm;


public enum TransactionType {
    BUY("Buy"),
    SELL("Sell");

    private String fieldDescription;

    TransactionType(String text) {
        fieldDescription = text;
    }

    public String getFieldDescription() {
        return fieldDescription;
    }
}
