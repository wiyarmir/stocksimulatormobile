package es.wiyarmir.stocksimulator.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;


public abstract class FilterableAdapter<Type> extends BaseAdapter {
    private TypeFilter mFilter;
    private Context context;
    private List<Type> mItems;
    private List<Type> filtered;

    public FilterableAdapter(Context ctx) {
        context = ctx;
        mItems = new ArrayList<Type>();
        filtered = new ArrayList<Type>();
    }

    public void clear() {
        mItems.clear();
    }

    public void clearFilter() {
        filtered.clear();
    }

    public void add(Type s) {
        mItems.add(s);
    }

    @Override
    public int getCount() {
        return filtered.size();
    }

    @Override
    public Object getItem(int i) {
        return filtered.get(i);
    }

    @Override
    public long getItemId(int i) {
        return filtered.get(i).hashCode();
    }

    public void addAll(List<Type> l2) {
        mItems.addAll(l2);
        filtered.addAll(l2);
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public List<Type> getAll() {
        return filtered;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new TypeFilter();
        }
        return mFilter;
    }

    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public class TypeFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // NOTE: this function is *always* called from a background thread, and
            // not the UI thread.
            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                ArrayList<Type> filt = new ArrayList<Type>();
                synchronized (mItems) {
                    //Collections.copy(lItems, Arrays.asList(items));
                }
                for (Type s : mItems) {
                    String m = s.toString();
                    if (m.toLowerCase().contains(constraint.toString()))
                        filt.add(s);
                }
                result.count = filt.size();
                result.values = filt;
            } else {
                synchronized (mItems) {
                    result.values = mItems;
                    result.count = mItems.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // NOTE: this function is *always* called from the UI thread.
            List<Type> filtered = (ArrayList<Type>) results.values;
            notifyDataSetChanged();
            clearFilter();
            if (filtered != null) {
                for (int i = 0; i < filtered.size(); i++) {
                    addFilter(filtered.get(i));
                }

            }
            notifyDataSetInvalidated();
        }
    }

    private void addFilter(Type Type) {
        filtered.add(Type);
    }

}