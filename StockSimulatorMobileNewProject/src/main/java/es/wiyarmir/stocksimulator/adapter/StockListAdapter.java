package es.wiyarmir.stocksimulator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import es.wiyarmir.stocksimulator.R;
import es.wiyarmir.stocksimulator.orm.Stock;
import es.wiyarmir.stocksimulator.util.Helper;

public class StockListAdapter extends FilterableAdapter<Stock> {
    static class ViewHolder {
        TextView ticker;
        TextView bidPrice;
        TextView askPrice;
        TextView move;
    }

    public StockListAdapter(Context context) {
        super(context);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.stocklistitem, null);
            holder = new ViewHolder();
            holder.ticker = (TextView) convertView.findViewById(R.id.textView_stockItemTicker);
            holder.bidPrice = (TextView) convertView.findViewById(R.id.textView_stockItemBidPrice);
            holder.askPrice = (TextView) convertView.findViewById(R.id.textView_stockItemAskPrice);
            holder.move = (TextView) convertView.findViewById(R.id.textView_stockItemMove);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Stock item = (Stock) getItem(position);

        if (item != null) {
            holder.ticker.setText(item.getTicker());
            holder.bidPrice.setText(String.format("%s", item.getCurrentStockPrice()));
            holder.askPrice.setText(String.format("%s", item.getCurrentAskPrice()));


            double move = Helper.calculateMove(item);
            holder.move.setText(String.format("%.3f%%", move));

            if (move < 0) {
                holder.move.setTextColor(getContext().getResources().getColor(android.R.color.holo_red_dark));
            } else if (move > 0) {
                holder.move.setTextColor(getContext().getResources().getColor(android.R.color.holo_green_dark));
            } else {
                holder.move.setTextColor(getContext().getResources().getColor(android.R.color.black));
            }
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.list_background_dark));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.list_background_light));
        }

        return convertView;
    }
}
