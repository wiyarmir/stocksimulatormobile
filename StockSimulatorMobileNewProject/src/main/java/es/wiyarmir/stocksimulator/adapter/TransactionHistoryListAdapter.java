package es.wiyarmir.stocksimulator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import es.wiyarmir.stocksimulator.R;
import es.wiyarmir.stocksimulator.orm.Transaction;
import es.wiyarmir.stocksimulator.util.Helper;


public class TransactionHistoryListAdapter extends FilterableAdapter<Transaction> {
    private static class ViewHolder {
        TextView ticker;
        TextView type;
        TextView quantity;
        TextView value;
    }

    public TransactionHistoryListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.transactionhistory_listitem, null);
            holder = new ViewHolder();
            holder.ticker = (TextView) convertView.findViewById(R.id.textView_stockItemTicker);
            holder.type = (TextView) convertView.findViewById(R.id.textView_stockItemType);
            holder.quantity = (TextView) convertView.findViewById(R.id.textView_stockItemQuantity);
            holder.value = (TextView) convertView.findViewById(R.id.textView_stockValue);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Transaction item = (Transaction) getItem(position);

        if (item != null) {
            holder.type.setText(item.getTransactionType().getFieldDescription());
            holder.quantity.setText(String.format("%d", item.getQuantity()));
            holder.ticker.setText(item.getTicker());
            holder.value.setText(String.format("%s", Helper.numberFormat.format(item.getTransactionStockPrice() * item.getQuantity())));
        }


        if (position % 2 == 0) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.list_background_dark));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.list_background_light));
        }

        return convertView;
    }
}
