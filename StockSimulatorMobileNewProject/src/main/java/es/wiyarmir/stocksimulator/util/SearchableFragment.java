package es.wiyarmir.stocksimulator.util;


public interface SearchableFragment {
    public void makeSearchHappen(String query);
}
