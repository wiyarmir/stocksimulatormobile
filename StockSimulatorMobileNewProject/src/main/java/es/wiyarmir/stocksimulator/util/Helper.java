package es.wiyarmir.stocksimulator.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import es.wiyarmir.stocksimulator.R;
import es.wiyarmir.stocksimulator.StockSimulatorApplication;
import es.wiyarmir.stocksimulator.orm.Stock;

import java.text.NumberFormat;
import java.util.Locale;

public class Helper {
    public static NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale(StockSimulatorApplication.getAppContext().getString(R.string.money_locale)));

    public static void FragmentReplacement(FragmentManager fragmentManager, Fragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content_frame, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static double calculateMove(Stock item) {
        double ret;
        if (item.getOpeningStockPrice() != 0) {
            ret = (item.getCurrentStockPrice() / item.getOpeningStockPrice() - 1) * 100;
        } else {
            ret = 0;
        }
        return ret;
    }
}
