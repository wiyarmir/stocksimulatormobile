package es.wiyarmir.stocksimulator.net;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.JsonReader;
import android.util.Log;

import es.wiyarmir.stocksimulator.orm.Portfolio;
import es.wiyarmir.stocksimulator.orm.Stock;
import es.wiyarmir.stocksimulator.orm.Transaction;
import es.wiyarmir.stocksimulator.orm.TransactionType;
import es.wiyarmir.stocksimulator.orm.User;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RESTClient {
    private HttpContext localContext = new BasicHttpContext();
    private HttpClient client = new DefaultHttpClient();
    private int userID;

    //String server = "http://localhost:8080/stock/";
    String server = "http://afternoon-bastion-4195.herokuapp.com/stock/";

    private RESTClient() {

    }

    private static RESTClient mInstance = null;

    public static synchronized RESTClient getInstance() {
        if (mInstance == null) {
            mInstance = new RESTClient();
        }
        return mInstance;
    }

    public int PostMarketOrderBuy(String ticker, int quantity) {
        Log.d("MARKET", "Attempting BUY market order");
        String endpoint = "%d/buy/%s/%d";
        HttpGet post = new HttpGet(server + String.format(endpoint, getUserId(), ticker, quantity));
        try {
            HttpResponse response = client.execute(post, localContext);
            return response.getStatusLine().getStatusCode();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int PostMarketOrderSell(String ticker, int quantity) {
        Log.d("MARKET", "Attempting SELL market order");
        String endpoint = "%d/sell/%s/%d";
        HttpGet post = new HttpGet(server + String.format(endpoint, getUserId(), ticker, quantity));
        try {
            HttpResponse response = client.execute(post,
                    localContext);
            return response.getStatusLine().getStatusCode();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private int getUserId() {
        return userID;
    }

    public void PostLimitOrder(TransactionType transactionType, String ticker, int amount,
                               double referencePrice, Date orderConstraint) {
        throw new RuntimeException();
    }

    public int PostEnhanceCredit() {
        Log.d("CREDIT", "Attempting to get a loan");
        String endpoint = "%d/enhanceCredit";
        HttpGet post = new HttpGet(server + String.format(endpoint, getUserId()));
        int ret = -1;
        try {
            HttpResponse response = client.execute(post,
                    localContext);
            JsonReader jsonReader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            jsonReader.beginObject();
            String name = jsonReader.nextName();
            if (name.equals("result")) {
                String s = jsonReader.nextString();
                if (s.equals("SUCCESS")) {
                    ret = 1;
                }
            } else {
                ret = 0;
            }
            jsonReader.endObject();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public Portfolio GetPortfolio() {
        String endpoint = "%d/portfolio";
        HttpGet get = new HttpGet(server + String.format(endpoint, getUserId()));
        Portfolio ret = new Portfolio();
        JsonReader jsonReader = null;
        HttpResponse response;
        try {
            response = client.execute(get, localContext);
            jsonReader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            ret = Portfolio.readPortfolio(jsonReader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }

    public double GetAccountBalance() {
        String endpoint = "%d/accountBalance";
        HttpGet get = new HttpGet(server + String.format(endpoint, getUserId()));
        JsonReader reader = null;
        HttpResponse response;
        double ret = 0;
        try {
            response = client.execute(get, localContext);
            reader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("accountBalance")) {
                    ret = reader.nextDouble();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public List<Transaction> GetTransactions() {
        String endpoint = "%d/transactions";
        HttpGet get = new HttpGet(server + String.format(endpoint, getUserId()));
        List<Transaction> ret = new ArrayList<Transaction>();
        JsonReader reader = null;
        HttpResponse response;
        try {
            response = client.execute(get, localContext);
            reader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("transactions")) {
                    reader.beginArray();
                    while (reader.hasNext()) {
                        ret.add(Transaction.readTransaction(reader));
                    }
                    reader.endArray();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }

    public List<Stock> GetExchange() {
        String endpoint = "exchange";
        HttpGet get = new HttpGet(server + String.format(endpoint, getUserId()));
        List<Stock> ret = new ArrayList<Stock>();
        JsonReader jsonReader = null;
        HttpResponse response;
        try {
            response = client.execute(get, localContext);
            jsonReader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            ret = readExchange(jsonReader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }

    private List<Stock> readExchange(JsonReader reader) throws IOException {
        List<Stock> ret = new ArrayList<Stock>();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("stocks")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    ret.add(Stock.readStock(reader));
                }
                reader.endArray();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return ret;

    }

    public List<Stock> GetExchangeTop() {
        String endpoint = "exchange/top";
        HttpGet get = new HttpGet(server + String.format(endpoint, getUserId()));
        List<Stock> ret = null;
        JsonReader jsonReader;
        HttpResponse response;
        try {
            response = client.execute(get, localContext);
            jsonReader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            ret = readExchange(jsonReader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }

    public List<User> GetLeaderboard() {
        String uri = server + "leaderboard";
        HttpGet get = new HttpGet(uri);
        List<User> ret = new ArrayList<User>();
        JsonReader jsonReader = null;
        HttpResponse response;
        try {
            response = client.execute(get, localContext);
            jsonReader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            ret = readLeaderboard(jsonReader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return ret;
    }

    private List<User> readLeaderboard(JsonReader reader) throws IOException {
        List<User> ret = new ArrayList<User>();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("users")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    ret.add(User.readUser(reader));
                }
                reader.endArray();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return ret;
    }

    /**
     * Ask for authorization
     *
     * @return userID or 0 on failure
     */
    public int GetAuthorization(String user, String pass) {
        String endpoint = "auth/%s/%s";
        HttpGet get = new HttpGet(server + String.format(endpoint, user, pass));
        Log.d("TAG", "Atempting login at " + get.getURI().toString());
        JsonReader reader = null;
        HttpResponse response;
        int ret = -3;
        try {
            response = client.execute(get, localContext);
            Log.d("LOGIN", "Response: " + response.getStatusLine().getStatusCode());
            reader = new JsonReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("userId")) {
                    ret = reader.nextInt();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return -2;
        }
        // Log.w("LOGIN", "UWAGA! We are not actually doing login");
        return ret;
    }

    public Bitmap GetChart(String ticker, String time) {
        URL newurl = null;
        Bitmap img = null;
        try {
            newurl = new URL("http://ichart.finance.yahoo.com/z?s=" + ticker + "&t=3d");
            img = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (img == null) {
                Log.d("REST", "Error loading image:" + newurl.toString());
            }
        }
        return img;
    }

    public static void setInstanceUserID(int ID) {
        getInstance().userID = ID;
    }

}
