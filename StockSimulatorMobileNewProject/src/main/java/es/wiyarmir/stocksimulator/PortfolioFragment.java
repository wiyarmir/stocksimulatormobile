package es.wiyarmir.stocksimulator;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import es.wiyarmir.stocksimulator.adapter.FilterableAdapter;
import es.wiyarmir.stocksimulator.net.RESTClient;
import es.wiyarmir.stocksimulator.orm.Portfolio;
import es.wiyarmir.stocksimulator.orm.Stock;
import es.wiyarmir.stocksimulator.util.Helper;
import es.wiyarmir.stocksimulator.util.SearchableFragment;


public class PortfolioFragment extends Fragment implements SearchableFragment {
    View v;
    private FilterableAdapter myAdapter;
    private TextView tBalance;
    private TextView tPenalty;
    private ListView myListView;
    private Button bLoan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.portfolio_fragment, null);

        myAdapter = new StocksFilterAdapter(getActivity());
        myListView = (ListView) v.findViewById(R.id.listView);
        myListView.setAdapter(myAdapter);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StockDetailFragment fragment = new StockDetailFragment();
                Bundle args = new Bundle();
                args.putSerializable("stock", (Stock) parent.getItemAtPosition(position));
                fragment.setArguments(args);

                Helper.FragmentReplacement(getActivity().getSupportFragmentManager(), fragment, true, null);
            }
        });

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        tBalance = (TextView) v.findViewById(R.id.valueBalance);
        tPenalty = (TextView) v.findViewById(R.id.valueScore);

        bLoan = (Button) v.findViewById(R.id.button_getLoan);

        bLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetLoanTask().execute();
            }
        });

        new PortfolioTask().execute();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void populateListView(Portfolio p) {

        if (p != null) {
            myAdapter.clear();
            myAdapter.addAll(p.getStocksList());
            tBalance.setText(Helper.numberFormat.format(p.getAccountBalance()));
            tPenalty.setText(Helper.numberFormat.format(p.getAccountPenalty()));
            if (p.getAccountPenalty() < 0) {
                tPenalty.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
        } else {
            Toast.makeText(StockSimulatorApplication.getAppContext(), "Server error.", Toast.LENGTH_LONG).show();
        }

        if (myAdapter.getCount() == 0) {
            v.findViewById(R.id.textView_NoStock).setVisibility(View.VISIBLE);
            TextView text = (TextView) v.findViewById(R.id.textView_NoStockLink);
            text.setVisibility(View.VISIBLE);
            /*
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Helper.FragmentReplacement(getActivity().getSupportFragmentManager(), new StockListFragment(), true, "StockList");
                    getActivity().getActionBar().setTitle("Stocks");
                }
            });
            */
        }
    }

    @Override
    public void makeSearchHappen(String query) {
        myAdapter.getFilter().filter(query);
    }

    public class StocksFilterAdapter extends FilterableAdapter<Stock> {
        public StocksFilterAdapter(Context ctx) {
            super(ctx);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = LayoutInflater.from(getContext()).inflate(R.layout.portfolio_list, null);
            }

            Stock p = (Stock) getItem(position);

            TextView tTick = (TextView) itemView.findViewById(R.id.textTicker);
            tTick.setText(p.getTicker());

            TextView tCompName = (TextView) itemView.findViewById(R.id.textCN);
            tCompName.setText(p.getCompanyName());

            TextView tQuantity = (TextView) itemView.findViewById(R.id.textQuant);
            tQuantity.setText(Integer.toString(p.getQuantity()));

            TextView tCurrPrice = (TextView) itemView.findViewById(R.id.textCurrPrice);
            tCurrPrice.setText(Helper.numberFormat.format(p.getCurrentStockPrice()));

            if (position % 2 == 0) {
                itemView.setBackgroundColor(getResources().getColor(R.color.list_background_dark));
            } else {
                itemView.setBackgroundColor(getResources().getColor(R.color.list_background_light));
            }

            return itemView;
        }
    }

    public class PortfolioTask extends AsyncTask<Void, Void, Portfolio> {

        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading portfolio...");
            pd.show();
        }


        @Override
        protected Portfolio doInBackground(Void... voids) {
            RESTClient r = RESTClient.getInstance();
            return r.GetPortfolio();
        }

        @Override
        protected void onPostExecute(Portfolio portfolio) {
            populateListView(portfolio);
            pd.dismiss();
        }
    }

    public class GetLoanTask extends AsyncTask<Void, Void, Integer> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Soliciting loan...");
            pd.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return RESTClient.getInstance().PostEnhanceCredit();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (integer > 0) {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Loan granted.", Toast.LENGTH_LONG).show();
                new RefreshBalanceTask().execute();
            } else {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Loan denied.", Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }

    public class RefreshBalanceTask extends AsyncTask<Void, Void, Double> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Refreshing account balance...");
            pd.show();
        }


        @Override
        protected Double doInBackground(Void... voids) {
            return RESTClient.getInstance().GetAccountBalance();
        }

        @Override
        protected void onPostExecute(Double balance) {
            tBalance.setText(Helper.numberFormat.format(balance));
            pd.dismiss();
        }
    }
}
