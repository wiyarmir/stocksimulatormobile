package es.wiyarmir.stocksimulator;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import es.wiyarmir.stocksimulator.net.RESTClient;
import es.wiyarmir.stocksimulator.orm.Stock;
import es.wiyarmir.stocksimulator.util.Helper;


public class StockDetailFragment extends Fragment {
    private ImageView chartView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.stock_detail_fragment, null);

        final Stock item = (Stock) getArguments().getSerializable("stock");

        ((TextView) v.findViewById(R.id.textView_stockItemAskPrice)).setText(String.format("%s", item.getCurrentAskPrice()));

        ((TextView) v.findViewById(R.id.textView_stockItemBidPrice)).setText(String.format("%s", item.getCurrentStockPrice()));

        ((TextView) v.findViewById(R.id.textView_openingPrice)).setText(String.format("%s", item.getOpeningStockPrice()));

        TextView text = (TextView) v.findViewById(R.id.textView_stockItemMove);

        double value = Helper.calculateMove(item);

        if (value != 0) {
            text.setText(String.format("%.3f%%", value));

            if (value < 0) {
                text.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            } else if (value > 0) {
                text.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            } else {
                text.setTextColor(getResources().getColor(android.R.color.black));
            }
        } else {
            text.setText("--- %");
        }

        TextView tQuantity = (TextView) v.findViewById(R.id.textView_amountOfStocks);
        ;
        if (item.getQuantity() > 0) {
            tQuantity.setText(String.format("%s", item.getQuantity()));
        } else {
            tQuantity.setVisibility(View.GONE);
            ((TextView) v.findViewById(R.id.stockTitle)).setVisibility(View.GONE);
        }
        ((TextView) v.findViewById(R.id.textView_ticker)).setText(String.format("%s", item.getTicker()));

        ((TextView) v.findViewById(R.id.textView_companyName)).setText(String.format("%s", item.getCompanyName()));

        ((Button) v.findViewById(R.id.button_action_buy)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StockDetailPopup popup = new StockDetailPopup();
                Bundle args = new Bundle();
                args.putSerializable("stock", item);
                args.putBoolean("isBuying", true);
                popup.setArguments(args);
                popup.show(getChildFragmentManager(), "stock_details");
            }
        });

        ((Button) v.findViewById(R.id.button_action_sell)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StockDetailPopup popup = new StockDetailPopup();
                Bundle args = new Bundle();
                args.putSerializable("stock", item);
                args.putBoolean("isBuying", false);
                popup.setArguments(args);
                popup.show(getChildFragmentManager(), "stock_details");
            }
        });

        chartView = (ImageView) v.findViewById(R.id.imageView_chart);

        new LoadChartTask().execute(item);

        return v;
    }

    public class LoadChartTask extends AsyncTask<Stock, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Stock... stocks) {
            Stock s = stocks[0];
            return RESTClient.getInstance().GetChart(s.getTicker(), "");
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                chartView.setImageDrawable(new BitmapDrawable(StockSimulatorApplication.getAppContext().getResources(), bitmap));
            }
        }
    }
}
