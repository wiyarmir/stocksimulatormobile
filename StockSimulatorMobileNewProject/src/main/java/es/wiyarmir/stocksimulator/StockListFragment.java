package es.wiyarmir.stocksimulator;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import es.wiyarmir.stocksimulator.adapter.StockListAdapter;
import es.wiyarmir.stocksimulator.net.RESTClient;
import es.wiyarmir.stocksimulator.orm.Stock;
import es.wiyarmir.stocksimulator.util.Helper;
import es.wiyarmir.stocksimulator.util.SearchableFragment;

import java.util.List;

public class StockListFragment extends Fragment implements SearchableFragment, ListView.OnItemClickListener {
    private TextView textAccountBalance;
    private Button buttonGetLoan;
    private ListView listView;
    private StockListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stock_list, null);

        listView = ((ListView) view.findViewById(R.id.listView));
        listView.setOnItemClickListener(this);
        adapter = new StockListAdapter(getActivity());
        listView.setAdapter(adapter);

        textAccountBalance = ((TextView) view.findViewById(R.id.textView_accountBalance));
        new ExchangeTask().execute();

        buttonGetLoan = (Button) view.findViewById(R.id.button_getLoan);
        buttonGetLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetLoanTask().execute();
            }
        });
        return view;
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        StockDetailFragment fragment = new StockDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable("stock", (Stock) adapter.getItem(position));
        fragment.setArguments(args);
        Helper.FragmentReplacement(getActivity().getSupportFragmentManager(), fragment, true, "Stock Detail");
    }

    @Override
    public void makeSearchHappen(String query) {
        adapter.getFilter().filter(query);
    }

    public class ExchangeTask extends AsyncTask<Void, Void, List<Stock>> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading exchange market...");
            pd.show();
            new RefreshBalanceTask().execute();
        }

        @Override
        protected List<Stock> doInBackground(Void... voids) {
            return RESTClient.getInstance().GetExchange();
        }

        @Override
        protected void onPostExecute(List<Stock> stocks) {
            if (stocks != null && !stocks.isEmpty()) {
                adapter.clear();
                adapter.addAll(stocks);
            } else {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Server connection error", Toast.LENGTH_LONG).show();

            }
            pd.dismiss();
        }
    }

    public class GetLoanTask extends AsyncTask<Void, Void, Integer> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Soliciting loan...");
            pd.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return RESTClient.getInstance().PostEnhanceCredit();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (integer > 0) {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Loan granted.", Toast.LENGTH_LONG).show();
                new RefreshBalanceTask().execute();
            } else {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Loan denied.", Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }

    public class RefreshBalanceTask extends AsyncTask<Void, Void, Double> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Refreshing account balance...");
            pd.show();
        }


        @Override
        protected Double doInBackground(Void... voids) {
            return RESTClient.getInstance().GetAccountBalance();
        }

        @Override
        protected void onPostExecute(Double balance) {
            textAccountBalance
                    .setText(Helper.numberFormat.format(balance));
            pd.dismiss();
        }
    }
}
