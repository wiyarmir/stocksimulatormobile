package es.wiyarmir.stocksimulator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import es.wiyarmir.stocksimulator.adapter.TransactionHistoryListAdapter;
import es.wiyarmir.stocksimulator.net.RESTClient;
import es.wiyarmir.stocksimulator.orm.Transaction;
import es.wiyarmir.stocksimulator.util.SearchableFragment;

import java.util.List;


public class TransactionHistoryFragment extends Fragment implements SearchableFragment {

    private TransactionHistoryListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.transaction_history_fragment, null);
        adapter = new TransactionHistoryListAdapter(getActivity());
        ListView lv = (ListView) view.findViewById(R.id.listView);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TransactionDetailPopup f = new TransactionDetailPopup();
                Bundle args = new Bundle();
                args.putSerializable("Transaction", (Transaction) adapter.getItem(position));
                f.setArguments(args);
                f.show(getChildFragmentManager(), "Transaction details");
                        //adapter.getItem(position);
            }
        });
        new TransactionHistoryTask().execute();
        return view;
    }

    @Override
    public void makeSearchHappen(String query) {
        adapter.getFilter().filter(query);
    }


    public class TransactionHistoryTask extends AsyncTask<Void, Void, List<Transaction>> {

        @Override
        protected List<Transaction> doInBackground(Void... voids) {
            return RESTClient.getInstance().GetTransactions();
        }

        @Override
        protected void onPostExecute(List<Transaction> transactions) {
            if (transactions != null && !transactions.isEmpty()) {
                adapter.clear();
                adapter.addAll(transactions);
            } else {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Server's down.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
