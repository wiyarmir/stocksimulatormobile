package es.wiyarmir.stocksimulator;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.wiyarmir.stocksimulator.net.RESTClient;
import es.wiyarmir.stocksimulator.orm.MarketOrder;
import es.wiyarmir.stocksimulator.orm.Stock;
import es.wiyarmir.stocksimulator.orm.TransactionType;

import java.text.DecimalFormat;


public class StockDetailPopup extends DialogFragment {
    Stock item;
    boolean isBuying;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.stock_transaction_popup, null);

        item = (Stock) getArguments().getSerializable("stock");
        isBuying = getArguments().getBoolean("isBuying");

        getDialog().setTitle(isBuying ? "Market Order - Buy" : "Market Order - Sell");

        final EditText editValue = (EditText) v.findViewById(R.id.editText_transaction_val_order);
        final EditText editQuantity = (EditText) v.findViewById(R.id.editText_stock_quant_order);

        editValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    if (editValue.hasFocus()) {
                        String number = editValue.getText().toString();

                        if (number.length() > 0) {
                            double value = Double.parseDouble(number);
                            double price;

                            if (isBuying) {
                                price = item.getCurrentAskPrice();
                            } else {
                                price = item.getCurrentStockPrice();
                            }

                            int quantity = (int) (value / price);
                            editQuantity.setText(String.format("%d", quantity));
                        } else {
                            editQuantity.setText(null);
                        }
                    }
                } catch (Exception e) {
                    android.widget.Toast.makeText(getActivity(), "Something happens with Value", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    if (editQuantity.hasFocus()) {
                        String number = editQuantity.getText().toString();
                        if (number.length() > 0) {
                            int amount = Integer.parseInt(number);
                            double price;

                            if (isBuying) {
                                price = item.getCurrentAskPrice();
                            } else {
                                price = item.getCurrentStockPrice();
                            }

                            double cost = amount * price;

                            DecimalFormat df = new DecimalFormat("#.##");
                            editValue.setText(String.format("%s", df.format(cost)));

                        } else {
                            editValue.setText(null);
                        }
                    }
                } catch (Exception e) {
                    android.widget.Toast.makeText(getActivity(), "Something happens with Quantity", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Button btn = (Button) v.findViewById(R.id.button_validate);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editQuantity.getText().toString().equals("")) {
                    new OrderTask().execute(new MarketOrder(item.getTicker(),
                            Integer.parseInt(editQuantity.getText().toString()),
                            isBuying ? TransactionType.BUY : TransactionType.SELL));
                }
            }
        });


        return v;
    }

    public class OrderTask extends AsyncTask<MarketOrder, Void, Integer> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {

            pd = new ProgressDialog(getActivity());
            pd.setMessage("Sending order to the exchange market...");
            pd.show();
            StockDetailPopup.this.dismiss();

        }

        @Override
        protected Integer doInBackground(MarketOrder... marketOrders) {
            int ret = -100;
            for (MarketOrder m : marketOrders) {
                if (m.getTransactionType() == TransactionType.BUY) {
                    ret = RESTClient.getInstance().PostMarketOrderBuy(m.getTicker(), m.getQuantity());
                } else {
                    ret = RESTClient.getInstance().PostMarketOrderSell(m.getTicker(), m.getQuantity());
                }
            }
            return ret;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result > 0) {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Order succeeded", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Order failed", Toast.LENGTH_SHORT).show();
            }
            pd.dismiss();
        }
    }
}
