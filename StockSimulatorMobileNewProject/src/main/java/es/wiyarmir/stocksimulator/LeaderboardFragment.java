package es.wiyarmir.stocksimulator;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import es.wiyarmir.stocksimulator.adapter.FilterableAdapter;
import es.wiyarmir.stocksimulator.net.RESTClient;
import es.wiyarmir.stocksimulator.orm.User;
import es.wiyarmir.stocksimulator.util.SearchableFragment;

import java.text.DecimalFormat;
import java.util.List;

public class LeaderboardFragment extends Fragment implements SearchableFragment {

    private LeaderAdapter myAdapter;
    TextView tPosition;
    TextView tUserName;
    TextView tUserScore;
    private ListView lv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.leaderboard_fragment, null);

        myAdapter = new LeaderAdapter(getActivity());

        lv = (ListView) v.findViewById(R.id.listView);
        lv.setAdapter(myAdapter);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        new LeaderboardTask().execute();
    }

    private void populateListView(final List<User> users) {
        if (users.size() > 0) {
            myAdapter.clear();
            myAdapter.addAll(users);
        } else {
            Toast.makeText(StockSimulatorApplication.getAppContext(), "Server's down.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void makeSearchHappen(String query) {
        myAdapter.getFilter().filter(query);
    }

    private class LeaderAdapter extends FilterableAdapter<User> {
        public LeaderAdapter(Context ctx) {
            super(ctx);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            if (itemView == null) {
                itemView = getActivity().getLayoutInflater().inflate(R.layout.leaderboardlistitem, null);
            }

            User user = (User) getItem(position);

            if (position % 2 == 0) {
                itemView.setBackgroundColor(getResources().getColor(R.color.list_background_dark));
            } else {
                itemView.setBackgroundColor(getResources().getColor(R.color.list_background_light));
            }

            tPosition = (TextView) itemView.findViewById(R.id.leader_position);
            tPosition.setText("" + (1 + position));

            tUserName = (TextView) itemView.findViewById(R.id.leader_username);
            tUserName.setText(user.getUsername());

            DecimalFormat df = new DecimalFormat("#.###");
            double score = user.getScore();
            tUserScore = (TextView) itemView.findViewById(R.id.leader_score);
            tUserScore.setText(String.format("%s", df.format(score)));

            return itemView;
        }
    }

    public class LeaderboardTask extends AsyncTask<Void, Void, List<User>> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading leaderboard...");
            pd.show();
        }

        @Override
        protected List<User> doInBackground(Void... voids) {
            RESTClient r = RESTClient.getInstance();
            return r.GetLeaderboard();
        }

        @Override
        protected void onPostExecute(List<User> users) {
            if (users != null && !users.isEmpty()) {
                populateListView(users);
            } else {
                Toast.makeText(StockSimulatorApplication.getAppContext(), "Server connection error", Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }
}
