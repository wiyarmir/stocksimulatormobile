package es.wiyarmir.stocksimulator;

import android.app.Application;
import android.content.Context;


public class StockSimulatorApplication extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        StockSimulatorApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return StockSimulatorApplication.context;
    }
}
