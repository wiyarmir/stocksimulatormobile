package es.wiyarmir.stocksimulator;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import es.wiyarmir.stocksimulator.orm.Transaction;
import es.wiyarmir.stocksimulator.util.Helper;

public class TransactionDetailPopup extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().setTitle("Transaction Details");

        Transaction item = (Transaction) getArguments().getSerializable("Transaction");

        View dialog = inflater.inflate(R.layout.transaction_detail_popup, null);
        TextView text = (TextView) dialog.findViewById(R.id.textView_ticker);
        text.setText(item.getTicker());

        text = (TextView) dialog.findViewById(R.id.textView_companyName);
        text.setText(item.getCompanyName());

        text = (TextView) dialog.findViewById(R.id.textView_transactionType);
        text.setText(item.getTransactionType().getFieldDescription());

        text = (TextView) dialog.findViewById(R.id.textView_orderType);
        text.setText(item.getOrderType().getFieldDescription());

        text = (TextView) dialog.findViewById(R.id.textView_amountOfStocks);
        text.setText(item.getQuantity() + "");

        text = (TextView) dialog.findViewById(R.id.textView_transactionPrice);
        text.setText(String.format("%s", Helper.numberFormat.format(item.getTransactionStockPrice())));

        text = (TextView) dialog.findViewById(R.id.textView_transactionValue);

        double value = item.getQuantity() * item.getTransactionStockPrice();
        text.setText(String.format("%s", Helper.numberFormat.format(value)));

        text = (TextView) dialog.findViewById(R.id.textView_transactionDate);
        text.setText(item.getExecutedAt().toString());

        dialog.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return dialog;
    }
}
